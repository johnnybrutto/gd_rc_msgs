#!/usr/bin/env python

import rospy
import gd_rc.msg
import gd_motors_brd.msg
#import std_msgs.msg
import geometry_msgs.msg

pub = None
#val1 = std_msgs.msg.String()
val1 = gd_motors_brd.msg.MotorSystemVariables()
val2 = geometry_msgs.msg.Vector3()

#CHANNEL	#RCNUMBER	#FUNCTION
#3			#1			UP/DOWN
#4			#2			PITCH
#5			#3			
#6			#4			YAW
#7			#5		
#8			#6

channel_height=3

channel_pitch=4
channel_yaw=6
#channel_roll=

val1.position = 70
#val1.velocity = 0
val1.k = 60
val1.ki = 0
val1.kd = 0

rc_min = -25000
rc_max = 25000

pitch_max=2.0
pitch_min=-2.0
roll_max=2.0
roll_min=-2.0
yaw_max=2.0
yaw_min=-2.0

gimbal_dead_band = 0.1

height_max=90 #cm
height_min=35 #cm

#vel_max=0.3 #cm^2
#vel_min=-0.3 #cm^2

roll=0.0
pitch=0.0
yaw=0.0



def subs_cb(msg):
    global pub, val1, val2
    global yaw, pitch

    val1.position=(msg.channel[channel_height].value-rc_min)*(height_max-height_min)/(rc_max-rc_min)+height_min
    #val1.velocity=(msg.channel[channel_velocity].value-rc_min)*(vel_max-vel_min)/(rc_max-rc_min)+vel_min
    pitch=(msg.channel[channel_pitch].value-rc_min)*(pitch_max-pitch_min)/(rc_max-rc_min)+pitch_min
    #roll=(msg.channel[channel_roll].value-rc_min)*(roll_max-roll_min)/(rc_max-rc_min)+roll_min
    yaw=(msg.channel[channel_yaw].value-rc_min)*(yaw_max-yaw_min)/(rc_max-rc_min)+yaw_min
    

if __name__ == '__main__':
    rospy.init_node('node_name')
    
    rospy.Subscriber('RC_read', gd_rc.msg.RCArray,subs_cb)

    pub = rospy.Publisher('motor_vars', gd_motors_brd.msg.MotorSystemVariables, queue_size=1)
    gimbal_pub = rospy.Publisher('gimbal/cmd_vel', geometry_msgs.msg.Vector3, queue_size=1)
    
    rate = rospy.Rate(50) # 10hz
    while not rospy.is_shutdown():
    #while True:
        #rospy.loginfo(val1.data)
        #print "here"
		rospy.loginfo(val1.position)
		
		pub.publish(val1)
		
		#dead band
		if abs(pitch)>gimbal_dead_band :
			val2.y=-pitch
		else:
			val2.y=0
			
		if abs(yaw)>gimbal_dead_band :
			val2.z=-yaw
		else:
			val2.z=0
			
		#if abs(roll)>gimbal_dead_band :
		#	val2.x=roll
		#else:
		#	val2.x=0
		
		gimbal_pub.publish(val2)	
		
		rate.sleep()
